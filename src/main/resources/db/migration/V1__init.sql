create sequence category_sequence start 1 increment 1;
create sequence course_sequence start 1 increment 1;
create sequence lesson_sequence start 1 increment 1;
create sequence teacher_sequence start 1 increment 1;
create sequence user_sequence start 1 increment 1;
create table app_user
(
  id         int8 not null default nextval('user_sequence'),
  email      varchar(255),
  password   varchar(255),
  role       varchar(255),
  teacher_id int8,
  primary key (id)
);
create table category
(
  id                 int8 not null default nextval('category_sequence'),
  name               varchar(255),
  parent_category_id int8,
  primary key (id)
);
create table course
(
  type            varchar(31) not null,
  id              int8        not null default nextval('course_sequence'),
  description     varchar(255),
  name            varchar(255),
  students_number int4,
  category_id     int8,
  curator_id      int8,
  primary key (id)
);
create table lesson
(
  id          int8 not null default nextval('lesson_sequence'),
  description varchar(255),
  name        varchar(255),
  start_time  timestamp,
  course_id   int8,
  teacher_id  int8,
  primary key (id)
);
create table offline_course
(
  address varchar(255),
  id      int8 not null,
  primary key (id)
);
create table online_course
(
  platform_link varchar(255),
  id            int8 not null,
  primary key (id)
);
create table teacher
(
  id        int8 not null default nextval('teacher_sequence'),
  age       bytea,
  full_name varchar(255),
  primary key (id)
);
alter table app_user
  add constraint app_user_teacher_fk foreign key (teacher_id) references teacher on delete cascade;
alter table category
  add constraint category_category_fk foreign key (parent_category_id) references category on delete set null;
alter table course
  add constraint course_category_fk foreign key (category_id) references category on delete set null;
alter table course
  add constraint course_teacher_fk foreign key (curator_id) references teacher on delete set null;
alter table lesson
  add constraint lesson_course_fk foreign key (course_id) references course on delete set null;
alter table lesson
  add constraint lesson_teacher_fk foreign key (teacher_id) references teacher on delete set null;
alter table offline_course
  add constraint offline_course_course_fk foreign key (id) references course on delete cascade;
alter table online_course
  add constraint online_course_course_fk foreign key (id) references course on delete cascade;
