package com.tinkoff.course.model.dto;

import com.tinkoff.course.model.CourseType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class CourseDto {
    private String name;
    private String description;
    private Integer studentsNumber;
    private Long categoryId;
    private Long curatorId;
    private List<Long> lessonsIds;
    private CourseType courseType;
    private Map<String, Object> details;
}
