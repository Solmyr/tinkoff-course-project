package com.tinkoff.course.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CategoryDto {
    private String name;
    private Long parentCategoryId;
}
