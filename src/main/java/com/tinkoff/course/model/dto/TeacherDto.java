package com.tinkoff.course.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Period;

@Data
@AllArgsConstructor
public class TeacherDto {
    private String fullName;
    private Period age;
}
