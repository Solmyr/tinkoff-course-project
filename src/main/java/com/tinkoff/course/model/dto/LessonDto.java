package com.tinkoff.course.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class LessonDto {
    private String name;
    private String description;
    private Long teacherId;
    private LocalDateTime startTime;
    private Long courseId;
}
