package com.tinkoff.course.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateLessonResponse {
    private Long id;
}
