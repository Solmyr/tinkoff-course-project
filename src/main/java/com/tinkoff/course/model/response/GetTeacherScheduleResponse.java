package com.tinkoff.course.model.response;

import com.tinkoff.course.model.dto.LessonDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class GetTeacherScheduleResponse {
    private List<LessonDto> schedule;
}
