package com.tinkoff.course.model;

public enum CourseType {
    ONLINE,
    OFFLINE
}
