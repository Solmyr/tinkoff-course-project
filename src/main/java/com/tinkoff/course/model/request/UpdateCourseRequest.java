package com.tinkoff.course.model.request;

import com.tinkoff.course.validation.constraint.CategoryExistsByIdConstraint;
import com.tinkoff.course.validation.constraint.TeacherExistsByIdConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateCourseRequest {
    private String name;
    private String description;
    private Integer studentsNumber;
    @CategoryExistsByIdConstraint
    private Long categoryId;
    @TeacherExistsByIdConstraint
    private Long curatorId;

    @NotNull(message = "Details field is mandatory")
    private Map<String, Object> details;
}
