package com.tinkoff.course.model.request;

import com.tinkoff.course.validation.constraint.CategoryExistsByIdConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateCategoryRequest {
    @NotBlank(message = "Name is mandatory")
    private String name;
    @CategoryExistsByIdConstraint
    private Long parentCategoryId;
}
