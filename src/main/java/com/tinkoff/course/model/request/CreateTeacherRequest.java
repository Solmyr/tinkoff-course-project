package com.tinkoff.course.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Period;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateTeacherRequest {
    @NotBlank(message = "Name is mandatory")
    private String fullName;
    @NotNull(message = "Age is mandatory")
    private Period age;
}
