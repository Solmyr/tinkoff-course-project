package com.tinkoff.course.model.request;

import com.tinkoff.course.validation.constraint.TeacherExistsByIdConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateLessonRequest {
    private String name;
    private String description;
    @TeacherExistsByIdConstraint
    private Long teacherId;
    private LocalDateTime startTime;
}
