package com.tinkoff.course.model.request;

import com.tinkoff.course.validation.constraint.CategoryExistsByIdConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateCategoryRequest {
    private String name;
    @CategoryExistsByIdConstraint
    private Long parentCategoryId;
}
