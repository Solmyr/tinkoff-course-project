package com.tinkoff.course.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Period;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateTeacherRequest {
    private String fullName;
    private Period age;
}
