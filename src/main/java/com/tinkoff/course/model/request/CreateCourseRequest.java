package com.tinkoff.course.model.request;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.validation.constraint.CategoryExistsByIdConstraint;
import com.tinkoff.course.validation.constraint.TeacherExistsByIdConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateCourseRequest {
    @NotBlank(message = "Name is mandatory")
    private String name;
    @NotBlank(message = "Description is mandatory")
    private String description;
    @NotNull(message = "Students number grade is mandatory")
    private Integer studentsNumber;
    @NotNull(message = "Category id is mandatory")
    @CategoryExistsByIdConstraint
    private Long categoryId;
    @NotNull(message = "Curator id is mandatory")
    @TeacherExistsByIdConstraint
    private Long curatorId;
    @NotNull(message = "Course type is mandatory")
    private CourseType courseType;
    @NotNull(message = "Details field is mandatory")
    private Map<String, Object> details;
}
