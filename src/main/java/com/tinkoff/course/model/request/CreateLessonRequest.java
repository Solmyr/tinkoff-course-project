package com.tinkoff.course.model.request;

import com.tinkoff.course.validation.constraint.CourseExistsByIdConstraint;
import com.tinkoff.course.validation.constraint.TeacherExistsByIdConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateLessonRequest {
    @NotBlank(message = "Name is mandatory")
    private String name;
    @NotBlank(message = "Description is mandatory")
    private String description;
    @NotNull(message = "Teacher id is mandatory")
    @TeacherExistsByIdConstraint
    private Long teacherId;
    @NotNull(message = "Start time is mandatory")
    private LocalDateTime startTime;
    @NotNull(message = "Course id is mandatory")
    @CourseExistsByIdConstraint
    private Long courseId;
}
