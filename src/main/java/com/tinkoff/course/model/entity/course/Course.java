package com.tinkoff.course.model.entity.course;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.Teacher;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "course")
@Getter
@Setter
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Course {
    @Id
    @SequenceGenerator(
            name = "course_sequence",
            sequenceName = "course_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "course_sequence"
    )
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "students_number")
    private Integer studentsNumber;
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private CourseType type;
    @ManyToOne
    private Category category;
    @ManyToOne
    private Teacher curator;

    @OneToMany(mappedBy = "course")
    private List<Lesson> lessons;

    public Course(
            String name,
            String description,
            Integer studentsNumber,
            Category category,
            Teacher curator,
            CourseType type
    ) {
        this.name = name;
        this.description = description;
        this.studentsNumber = studentsNumber;
        this.category = category;
        this.curator = curator;
        this.type = type;
        this.lessons = new ArrayList<>();
    }

    @PreRemove
    private void removeAssociations() {
        for (Lesson lesson : lessons) {
            lesson.setCourse(null);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Course course)) {
            return false;
        }
        return id.equals(course.getId());
    }
}
