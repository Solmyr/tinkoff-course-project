package com.tinkoff.course.model.entity;

import com.tinkoff.course.model.entity.course.Course;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "teacher")
@Getter
@Setter
@NoArgsConstructor
public class Teacher {
    @Id
    @SequenceGenerator(
            name = "teacher_sequence",
            sequenceName = "teacher_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "teacher_sequence"
    )
    private Long id;
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "age")
    private Period age;
    @OneToMany(mappedBy = "curator")
    private List<Course> courses;
    @OneToMany(mappedBy = "teacher")
    private List<Lesson> lessons;

    public Teacher(String fullName, Period age) {
        this.fullName = fullName;
        this.age = age;
        this.courses = new ArrayList<>();
        this.lessons = new ArrayList<>();
    }

    @PreRemove
    private void removeAssociations() {
        for (Course course : courses) {
            course.setCurator(null);
        }
        for (Lesson lesson : lessons) {
            lesson.setTeacher(null);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Teacher teacher)) {
            return false;
        }
        return id.equals(teacher.getId());
    }
}
