package com.tinkoff.course.model.entity;

import com.tinkoff.course.model.entity.course.Course;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity(name = "category")
@Getter
@Setter
@NoArgsConstructor
public class Category {
    @Id
    @SequenceGenerator(
            name = "category_sequence",
            sequenceName = "category_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "category_sequence"
    )
    private Long id;
    private String name;
    @ManyToOne
    private Category parentCategory;
    @OneToMany(mappedBy = "parentCategory")
    private List<Category> childCategories;
    @OneToMany(mappedBy = "category")
    private List<Course> courses;

    public Category(String name, Category parentCategory) {
        this.name = name;
        this.parentCategory = parentCategory;
    }

    public Category(String name) {
        this.name = name;
    }

    @PreRemove
    private void removeAssociations() {
        for (Course course : courses) {
            course.setCategory(null);
        }
        for (Category category : childCategories) {
            category.setParentCategory(null);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Category category)) {
            return false;
        }
        return id.equals(category.getId());
    }
}
