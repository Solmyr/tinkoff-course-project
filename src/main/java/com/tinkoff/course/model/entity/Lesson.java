package com.tinkoff.course.model.entity;

import com.tinkoff.course.model.entity.course.Course;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "lesson")
@Getter
@Setter
@NoArgsConstructor
public class Lesson {
    @Id
    @SequenceGenerator(
            name = "lesson_sequence",
            sequenceName = "lesson_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "lesson_sequence"
    )
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @ManyToOne
    private Teacher teacher;
    @Column(name = "start_time")
    private LocalDateTime startTime;
    @ManyToOne
    private Course course;

    public Lesson(
            String name,
            String description,
            Teacher teacher,
            LocalDateTime startTime,
            Course course
    ) {
        this.name = name;
        this.description = description;
        this.teacher = teacher;
        this.startTime = startTime;
        this.course = course;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Lesson lesson)) {
            return false;
        }
        return id.equals(lesson.getId());
    }
}
