package com.tinkoff.course.model.entity.course;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Teacher;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "offline_course")
@Getter
@Setter
@NoArgsConstructor
public class OfflineCourse extends Course {
    @Column(name = "address")
    private String address;

    public OfflineCourse(
            String name,
            String description,
            Integer studentsNumber,
            Category category,
            Teacher curator,
            String address
    ) {
        super(name, description, studentsNumber, category, curator, CourseType.OFFLINE);
        this.address = address;
    }
}
