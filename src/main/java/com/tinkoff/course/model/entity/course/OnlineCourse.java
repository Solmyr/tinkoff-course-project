package com.tinkoff.course.model.entity.course;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Teacher;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "online_course")
@Getter
@Setter
@NoArgsConstructor
public class OnlineCourse extends Course {
    @Column(name = "platform_link")
    private String platformLink;

    public OnlineCourse(
            String name,
            String description,
            Integer studentsNumber,
            Category category,
            Teacher curator,
            String platformLink
    ) {
        super(name, description, studentsNumber, category, curator, CourseType.ONLINE);
        this.platformLink = platformLink;
    }
}
