package com.tinkoff.course.controller;

import com.tinkoff.course.model.dto.CategoryDto;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.request.CreateCategoryRequest;
import com.tinkoff.course.model.request.UpdateCategoryRequest;
import com.tinkoff.course.model.response.CreateCategoryResponse;
import com.tinkoff.course.service.category.CreateCategoryService;
import com.tinkoff.course.service.category.DeleteCategoryByIdService;
import com.tinkoff.course.service.category.GetCategoryByIdService;
import com.tinkoff.course.service.category.UpdateCategoryService;
import com.tinkoff.course.validation.constraint.CategoryExistsByIdConstraint;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Validated
@RequestMapping("/category")
@RequiredArgsConstructor
public class CategoryController {
    private final CreateCategoryService createCategoryService;
    private final GetCategoryByIdService getCategoryByIdService;
    private final DeleteCategoryByIdService deleteCategoryByIdService;
    private final UpdateCategoryService updateCategoryService;

    @PostMapping
    public ResponseEntity<CreateCategoryResponse> create(
            @Valid @RequestBody CreateCategoryRequest createCategoryRequest
    ) {
        Category parentCategory = null;
        if (createCategoryRequest.getParentCategoryId() != null) {
            parentCategory = getCategoryByIdService.get(createCategoryRequest.getParentCategoryId());
        }
        Category category = createCategoryService.create(
                createCategoryRequest.getName(),
                parentCategory
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(
                new CreateCategoryResponse(
                        category.getId()
                )
        );
    }

    @GetMapping("/{categoryId}")
    public ResponseEntity<CategoryDto> get(
            @CategoryExistsByIdConstraint @PathVariable Long categoryId
    ) {
        Category category = getCategoryByIdService.get(categoryId);
        Category parentCategory = category.getParentCategory();
        Long parentCategoryId = null;
        if (parentCategory != null) {
            parentCategoryId = parentCategory.getId();
        }
        return ResponseEntity.ok().body(
                new CategoryDto(
                        category.getName(),
                        parentCategoryId
                )
        );
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<?> delete(
            @CategoryExistsByIdConstraint @PathVariable Long categoryId
    ) {
        deleteCategoryByIdService.delete(categoryId);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{categoryId}")
    public ResponseEntity<?> update(
            @CategoryExistsByIdConstraint @PathVariable Long categoryId,
            @Valid @RequestBody UpdateCategoryRequest updateCategoryRequest
    ) {
        Category category = getCategoryByIdService.get(categoryId);
        Category parentCategory = null;
        if (updateCategoryRequest.getParentCategoryId() != null) {
            parentCategory = getCategoryByIdService.get(updateCategoryRequest.getParentCategoryId());
        }
        updateCategoryService.update(
                category,
                updateCategoryRequest.getName(),
                parentCategory
        );
        return ResponseEntity.ok().build();
    }
}
