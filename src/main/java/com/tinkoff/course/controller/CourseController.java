package com.tinkoff.course.controller;

import com.tinkoff.course.model.dto.CourseDto;
import com.tinkoff.course.model.entity.User;
import com.tinkoff.course.model.entity.course.Course;
import com.tinkoff.course.model.request.CreateCourseRequest;
import com.tinkoff.course.model.request.UpdateCourseRequest;
import com.tinkoff.course.model.response.CreateCourseResponse;
import com.tinkoff.course.service.category.GetCategoryByIdService;
import com.tinkoff.course.service.course.CopyCourseService;
import com.tinkoff.course.service.course.DeleteCourseService;
import com.tinkoff.course.service.course.get.GetCourseByIdService;
import com.tinkoff.course.service.course.create.CreateCourseService;
import com.tinkoff.course.service.course.get.details.GetCourseWithDetailsByIdService;
import com.tinkoff.course.service.course.update.UpdateCourseService;
import com.tinkoff.course.service.teacher.GetTeacherByIdService;
import com.tinkoff.course.validation.constraint.CourseExistsByIdConstraint;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Validated
@RequestMapping("/course")
@RequiredArgsConstructor
public class CourseController {
    private final CreateCourseService createCourseService;
    private final GetCategoryByIdService getCategoryByIdService;
    private final GetTeacherByIdService getTeacherByIdService;
    private final GetCourseWithDetailsByIdService getCourseWithDetailsByIdService;
    private final DeleteCourseService deleteCourseService;
    private final GetCourseByIdService getCourseByIdService;
    private final UpdateCourseService updateCourseService;
    private final CopyCourseService copyCourseService;

    @PostMapping
    public ResponseEntity<CreateCourseResponse> create(
            @Valid @RequestBody CreateCourseRequest createCourseRequest
    ) {
        Course course = createCourseService.create(
                createCourseRequest.getName(),
                createCourseRequest.getDescription(),
                createCourseRequest.getStudentsNumber(),
                getCategoryByIdService.get(createCourseRequest.getCategoryId()),
                getTeacherByIdService.get(createCourseRequest.getCuratorId()),
                createCourseRequest.getCourseType(),
                createCourseRequest.getDetails()
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(
                new CreateCourseResponse(course.getId())
        );
    }

    @PostMapping("/{courseId}/copy")
    public ResponseEntity<CreateCourseResponse> createCopy(
            @CourseExistsByIdConstraint @PathVariable Long courseId,
            @AuthenticationPrincipal User user
    ) {
        CourseDto courseDto = getCourseWithDetailsByIdService.get(courseId);
        Course copy = copyCourseService.copy(courseDto, user.getTeacher());
        return ResponseEntity.status(HttpStatus.CREATED).body(
                new CreateCourseResponse(
                        copy.getId()
                )
        );
    }

    @GetMapping("/{courseId}")
    public ResponseEntity<CourseDto> getById(
            @CourseExistsByIdConstraint @PathVariable Long courseId
    ) {
        return ResponseEntity.ok().body(
                getCourseWithDetailsByIdService.get(courseId)
        );
    }

    @DeleteMapping("/{courseId}")
    public ResponseEntity<?> delete(
            @CourseExistsByIdConstraint @PathVariable Long courseId
    ) {
        deleteCourseService.delete(getCourseByIdService.get(courseId));
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{courseId}")
    public ResponseEntity<?> update(
            @CourseExistsByIdConstraint @PathVariable Long courseId,
            @Valid @RequestBody UpdateCourseRequest updateCourseRequest
    ) {
        updateCourseService.update(
                getCourseByIdService.get(courseId),
                updateCourseRequest.getName(),
                updateCourseRequest.getDescription(),
                updateCourseRequest.getStudentsNumber(),
                getCategoryByIdService.get(updateCourseRequest.getCategoryId()),
                getTeacherByIdService.get(updateCourseRequest.getCuratorId()),
                updateCourseRequest.getDetails()
        );
        return ResponseEntity.ok().build();
    }
}
