package com.tinkoff.course.controller;

import com.tinkoff.course.model.dto.LessonDto;
import com.tinkoff.course.model.dto.TeacherDto;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.request.CreateTeacherRequest;
import com.tinkoff.course.model.request.UpdateTeacherRequest;
import com.tinkoff.course.model.response.CreateTeacherResponse;
import com.tinkoff.course.model.response.GetTeacherScheduleResponse;
import com.tinkoff.course.service.teacher.*;
import com.tinkoff.course.validation.constraint.TeacherExistsByIdConstraint;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Validated
@RequestMapping("/teacher")
@RequiredArgsConstructor
public class TeacherController {
    private final CreateTeacherService createTeacherService;
    private final GetTeacherByIdService getTeacherByIdService;
    private final DeleteTeacherByIdService deleteTeacherByIdService;
    private final UpdateTeacherService updateTeacherService;
    private final LoadTeacherByIdService loadTeacherByIdService;

    @PostMapping
    public ResponseEntity<CreateTeacherResponse> create(
            @Valid @RequestBody CreateTeacherRequest createTeacherRequest
    ) {
        Teacher teacher = createTeacherService.create(
                createTeacherRequest.getFullName(),
                createTeacherRequest.getAge()
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(
                new CreateTeacherResponse(teacher.getId())
        );
    }

    @GetMapping("/{teacherId}")
    public ResponseEntity<TeacherDto> get(
            @TeacherExistsByIdConstraint @PathVariable Long teacherId
    ) {
        Teacher teacher = getTeacherByIdService.get(teacherId);
        return ResponseEntity.ok().body(
                new TeacherDto(
                        teacher.getFullName(),
                        teacher.getAge()
                )
        );
    }

    @GetMapping("/{teacherId}/schedule")
    public ResponseEntity<?> getSchedule(
            @TeacherExistsByIdConstraint @PathVariable Long teacherId
    ) {
        Teacher teacher = loadTeacherByIdService.load(teacherId);
        return ResponseEntity.ok().body(
                new GetTeacherScheduleResponse(
                        teacher
                                .getLessons()
                                .stream()
                                .map(l -> new LessonDto(
                                        l.getName(),
                                        l.getDescription(),
                                        l.getTeacher().getId(),
                                        l.getStartTime(),
                                        l.getCourse().getId()
                                ))
                                .toList()
                )
        );
    }

    @DeleteMapping("/{teacherId}")
    public ResponseEntity<?> delete(
            @TeacherExistsByIdConstraint @PathVariable Long teacherId
    ) {
        deleteTeacherByIdService.delete(teacherId);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{teacherId}")
    public ResponseEntity<?> put(
            @TeacherExistsByIdConstraint @PathVariable Long teacherId,
            @RequestBody UpdateTeacherRequest updateTeacherRequest
    ) {
        updateTeacherService.update(
                getTeacherByIdService.get(teacherId),
                updateTeacherRequest.getFullName(),
                updateTeacherRequest.getAge()
        );
        return ResponseEntity.ok().build();
    }
}
