package com.tinkoff.course.controller;

import com.tinkoff.course.model.dto.LessonDto;
import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.Course;
import com.tinkoff.course.model.request.CreateLessonRequest;
import com.tinkoff.course.model.request.UpdateLessonRequest;
import com.tinkoff.course.model.response.CreateLessonResponse;
import com.tinkoff.course.service.course.get.GetCourseByIdService;
import com.tinkoff.course.service.lesson.CreateLessonService;
import com.tinkoff.course.service.lesson.DeleteLessonService;
import com.tinkoff.course.service.lesson.GetLessonByIdService;
import com.tinkoff.course.service.lesson.UpdateLessonService;
import com.tinkoff.course.service.teacher.GetTeacherByIdService;
import com.tinkoff.course.validation.constraint.LessonExistsByIdConstraint;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Validated
@RequestMapping("/lesson")
@RequiredArgsConstructor
public class LessonController {
    private final CreateLessonService createLessonService;
    private final GetTeacherByIdService getTeacherByIdService;
    private final GetLessonByIdService getLessonByIdService;
    private final DeleteLessonService deleteLessonService;
    private final UpdateLessonService updateLessonService;
    private final GetCourseByIdService getCourseByIdService;

    @PostMapping
    public ResponseEntity<CreateLessonResponse> create(
            @Valid @RequestBody CreateLessonRequest createLessonRequest
    ) {
        Course course = getCourseByIdService.get(createLessonRequest.getCourseId());
        Lesson lesson = createLessonService.create(
                createLessonRequest.getName(),
                createLessonRequest.getDescription(),
                getTeacherByIdService.get(createLessonRequest.getTeacherId()),
                createLessonRequest.getStartTime(),
                course
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(
                new CreateLessonResponse(
                        lesson.getId()
                )
        );
    }

    @GetMapping("/{lessonId}")
    public ResponseEntity<LessonDto> get(
            @LessonExistsByIdConstraint @PathVariable Long lessonId
    ) {
        Lesson lesson = getLessonByIdService.get(lessonId);
        return ResponseEntity.ok().body(
                new LessonDto(
                        lesson.getName(),
                        lesson.getDescription(),
                        lesson.getTeacher().getId(),
                        lesson.getStartTime(),
                        lesson.getCourse().getId()
                )
        );
    }

    @DeleteMapping("/{lessonId}")
    public ResponseEntity<?> delete(
            @LessonExistsByIdConstraint @PathVariable Long lessonId
    ) {
        deleteLessonService.delete(getLessonByIdService.get(lessonId));
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{lessonId}")
    public ResponseEntity<?> update(
            @LessonExistsByIdConstraint @PathVariable Long lessonId,
            @Valid @RequestBody UpdateLessonRequest updateLessonRequest
    ) {
        Lesson lesson = getLessonByIdService.get(lessonId);
        Teacher newTeacher = getTeacherByIdService.get(updateLessonRequest.getTeacherId());
        updateLessonService.update(
                lesson,
                updateLessonRequest.getName(),
                updateLessonRequest.getDescription(),
                newTeacher,
                updateLessonRequest.getStartTime()
        );
        return ResponseEntity.ok().build();
    }
}
