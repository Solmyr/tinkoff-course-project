package com.tinkoff.course.repository.course;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.course.Course;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    @EntityGraph(
            type = EntityGraphType.FETCH,
            attributePaths = {
                    "lessons"
            }
    )
    Optional<Course> findById(Long id);

    @Query("select c.type from course c where c.id = (:id)")
    CourseType getCourseTypeById(Long id);
}
