package com.tinkoff.course.repository.course;

import com.tinkoff.course.model.entity.course.OfflineCourse;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OfflineCourseRepository extends JpaRepository<OfflineCourse, Long> {
    @EntityGraph(
            type = EntityGraph.EntityGraphType.FETCH,
            attributePaths = {
                    "lessons"
            }
    )
    Optional<OfflineCourse> findById(Long id);
}
