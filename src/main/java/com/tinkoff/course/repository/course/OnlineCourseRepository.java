package com.tinkoff.course.repository.course;

import com.tinkoff.course.model.entity.course.OnlineCourse;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OnlineCourseRepository extends JpaRepository<OnlineCourse, Long> {
    @EntityGraph(
            type = EntityGraph.EntityGraphType.FETCH,
            attributePaths = {
                    "lessons"
            }
    )
    Optional<OnlineCourse> findById(Long id);
}
