package com.tinkoff.course.service.category;

import com.tinkoff.course.model.entity.Category;

public interface GetCategoryByIdService {
    Category get(Long id);
}
