package com.tinkoff.course.service.category;

public interface CategoryExistsByIdService {
    boolean exists(Long id);
}
