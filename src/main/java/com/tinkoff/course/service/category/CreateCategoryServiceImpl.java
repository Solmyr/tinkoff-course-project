package com.tinkoff.course.service.category;

import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreateCategoryServiceImpl implements CreateCategoryService {
    private final CategoryRepository categoryRepository;

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_TEACHER')")
    public Category create(String name, Category parentCategory) {
        return categoryRepository.save(
                new Category(
                        name, parentCategory
                )
        );
    }
}
