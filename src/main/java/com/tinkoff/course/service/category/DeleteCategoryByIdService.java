package com.tinkoff.course.service.category;

public interface DeleteCategoryByIdService {
    void delete(Long id);
}
