package com.tinkoff.course.service.category;

import com.tinkoff.course.model.entity.Category;

public interface CreateCategoryService {
    Category create(String name, Category parentCategory);
}
