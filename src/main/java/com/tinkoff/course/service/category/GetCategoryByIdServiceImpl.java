package com.tinkoff.course.service.category;

import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetCategoryByIdServiceImpl implements GetCategoryByIdService {
    private final CategoryRepository categoryRepository;

    @Override
    public Category get(Long id) {
        return categoryRepository.getById(id);
    }
}
