package com.tinkoff.course.service.category;

import com.tinkoff.course.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteCategoryByIdServiceImpl implements DeleteCategoryByIdService {
    private final CategoryRepository categoryRepository;

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_TEACHER')")
    public void delete(Long id) {
        categoryRepository.deleteById(id);
    }
}
