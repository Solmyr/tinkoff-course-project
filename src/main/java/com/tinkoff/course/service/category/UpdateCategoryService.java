package com.tinkoff.course.service.category;

import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.validation.constraint.UpdateCategoryParentConstraint;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;

@Validated
public interface UpdateCategoryService {
    @Transactional
    @UpdateCategoryParentConstraint
    void update(Category category, String newName, Category newParentCategory);
}
