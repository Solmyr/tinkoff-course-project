package com.tinkoff.course.service.category;

import com.tinkoff.course.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoryExistsByIdServiceImpl implements CategoryExistsByIdService {
    private final CategoryRepository categoryRepository;

    @Override
    public boolean exists(Long id) {
        return categoryRepository.existsById(id);
    }
}
