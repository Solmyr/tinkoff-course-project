package com.tinkoff.course.service.category;

import com.tinkoff.course.model.entity.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class UpdateCategoryServiceImpl implements UpdateCategoryService {
    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_TEACHER')")
    public void update(Category category, String newName, Category newParentCategory) {
        if (newName != null) {
            category.setName(newName);
        }

        if (newParentCategory != null) {
            category.setParentCategory(newParentCategory);
        }
    }
}
