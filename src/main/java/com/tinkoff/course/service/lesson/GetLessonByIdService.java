package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;

public interface GetLessonByIdService {
    Lesson get(Long id);
}
