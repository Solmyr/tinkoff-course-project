package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;

import java.util.List;

public interface GetLessonListByIdService {
    List<Lesson> get(List<Long> ids);
}
