package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.repository.LessonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetLessonByIdServiceImpl implements GetLessonByIdService {
    private final LessonRepository lessonRepository;

    @Override
    public Lesson get(Long id) {
        return lessonRepository.getById(id);
    }
}
