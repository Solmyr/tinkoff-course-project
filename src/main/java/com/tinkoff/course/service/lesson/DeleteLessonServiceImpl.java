package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.repository.LessonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteLessonServiceImpl implements DeleteLessonService {
    private final LessonRepository lessonRepository;

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_TEACHER') " +
            "and #lesson.course.curator.id == principal.teacher.id")
    public void delete(Lesson lesson) {
        lessonRepository.delete(lesson);
    }
}
