package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.Course;

import java.time.LocalDateTime;

public interface CreateLessonService {
    Lesson create(
            String name,
            String description,
            Teacher teacher,
            LocalDateTime startTime,
            Course course
    );
}
