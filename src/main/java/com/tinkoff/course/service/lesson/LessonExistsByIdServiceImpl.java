package com.tinkoff.course.service.lesson;

import com.tinkoff.course.repository.LessonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LessonExistsByIdServiceImpl implements LessonExistsByIdService {
    private final LessonRepository lessonRepository;

    @Override
    public boolean exists(Long id) {
        return lessonRepository.existsById(id);
    }
}
