package com.tinkoff.course.service.lesson;

public interface LessonExistsByIdService {
    boolean exists(Long id);
}
