package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.course.Course;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CopyLessonServiceImpl implements CopyLessonService {
    private final CreateLessonService createLessonService;

    @Override
    public Lesson copy(Lesson lesson, Course newCourse) {
        return createLessonService.create(
                lesson.getName(),
                lesson.getDescription(),
                lesson.getTeacher(),
                LocalDateTime.of(1, 1, 1, 0, 0),
                newCourse
        );
    }
}
