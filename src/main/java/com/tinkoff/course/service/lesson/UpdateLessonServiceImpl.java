package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.Teacher;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class UpdateLessonServiceImpl implements UpdateLessonService {
    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN') " +
            "or hasRole('ROLE_TEACHER') and #lesson.course.curator.id == principal.teacher.id")
    public void update(
            Lesson lesson,
            String newName,
            String newDescription,
            Teacher newTeacher,
            LocalDateTime newStartTime
    ) {
        if (newName != null) {
            lesson.setName(newName);
        }

        if (newDescription != null) {
            lesson.setDescription(newDescription);
        }

        if (newTeacher != null) {
            lesson.setTeacher(newTeacher);
        }

        if (newStartTime != null) {
            lesson.setStartTime(newStartTime);
        }
    }
}
