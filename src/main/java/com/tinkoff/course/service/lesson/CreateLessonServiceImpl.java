package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.Course;
import com.tinkoff.course.repository.LessonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CreateLessonServiceImpl implements CreateLessonService {
    private final LessonRepository lessonRepository;

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN') or " +
            "hasRole('ROLE_TEACHER') and #course.curator.id == principal.teacher.id")
    public Lesson create(
            String name,
            String description,
            Teacher teacher,
            LocalDateTime startTime,
            Course course
    ) {
        return lessonRepository.save(
                new Lesson(
                        name,
                        description,
                        teacher,
                        startTime,
                        course
                )
        );
    }
}
