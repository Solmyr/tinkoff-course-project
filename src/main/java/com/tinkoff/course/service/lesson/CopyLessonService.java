package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.course.Course;

public interface CopyLessonService {
    Lesson copy(Lesson lesson, Course newCourse);
}
