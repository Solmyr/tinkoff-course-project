package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.repository.LessonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GetLessonListByIdServiceImpl implements GetLessonListByIdService {
    private final LessonRepository lessonRepository;

    @Override
    public List<Lesson> get(List<Long> ids) {
        return lessonRepository.findAllByIdIn(ids);
    }
}
