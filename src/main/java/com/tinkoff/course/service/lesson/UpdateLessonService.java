package com.tinkoff.course.service.lesson;

import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.Teacher;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

public interface UpdateLessonService {
    @Transactional
    void update(
            Lesson lesson,
            String newName,
            String newDescription,
            Teacher newTeacher,
            LocalDateTime newStartTime
    );
}
