package com.tinkoff.course.service.course.get;

import com.tinkoff.course.model.entity.course.OnlineCourse;

import javax.transaction.Transactional;

public interface LoadOnlineCourseByIdService {
    @Transactional
    OnlineCourse load(Long id);
}
