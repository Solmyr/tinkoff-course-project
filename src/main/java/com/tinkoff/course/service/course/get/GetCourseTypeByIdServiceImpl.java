package com.tinkoff.course.service.course.get;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.repository.course.CourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetCourseTypeByIdServiceImpl implements GetCourseTypeByIdService {
    private final CourseRepository courseRepository;

    @Override
    public CourseType get(Long id) {
        return courseRepository.getCourseTypeById(id);
    }
}
