package com.tinkoff.course.service.course;

import com.tinkoff.course.model.dto.CourseDto;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.Course;

public interface CopyCourseService {
    Course copy(CourseDto courseDto, Teacher newCurator);
}
