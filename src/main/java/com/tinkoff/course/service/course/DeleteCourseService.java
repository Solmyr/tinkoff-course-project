package com.tinkoff.course.service.course;

import com.tinkoff.course.model.entity.course.Course;

public interface DeleteCourseService {
    void delete(Course course);
}
