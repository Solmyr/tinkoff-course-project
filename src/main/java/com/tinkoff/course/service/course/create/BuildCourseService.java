package com.tinkoff.course.service.course.create;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.Course;

import java.util.Map;

public interface BuildCourseService {
    Course create(
            String name,
            String description,
            Integer studentsNumber,
            Category category,
            Teacher curator,
            Map<String, Object> details
    );
    CourseType getCourseType();
}
