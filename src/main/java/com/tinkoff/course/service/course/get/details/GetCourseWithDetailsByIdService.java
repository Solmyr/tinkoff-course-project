package com.tinkoff.course.service.course.get.details;

import com.tinkoff.course.model.dto.CourseDto;

public interface GetCourseWithDetailsByIdService {
    CourseDto get(Long id);
}
