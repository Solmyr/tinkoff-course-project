package com.tinkoff.course.service.course.get.details;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.dto.CourseDto;
import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.course.OfflineCourse;
import com.tinkoff.course.service.course.get.LoadOfflineCourseByIdService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class GetOfflineCourseDtoByIdService implements GetTypedCourseByIdService {
    private final LoadOfflineCourseByIdService loadOfflineCourseByIdService;

    @Override
    public CourseDto get(Long id) {
        OfflineCourse course = loadOfflineCourseByIdService.load(id);
        return new CourseDto(
                course.getName(),
                course.getDescription(),
                course.getStudentsNumber(),
                course.getCategory().getId(),
                course.getCurator().getId(),
                course.getLessons().stream().map(Lesson::getId).toList(),
                getCourseType(),
                Map.of("address", course.getAddress())
        );
    }

    @Override
    public CourseType getCourseType() {
        return CourseType.OFFLINE;
    }
}
