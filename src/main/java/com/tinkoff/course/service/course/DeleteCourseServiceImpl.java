package com.tinkoff.course.service.course;

import com.tinkoff.course.model.entity.course.Course;
import com.tinkoff.course.repository.course.CourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteCourseServiceImpl implements DeleteCourseService {
    private final CourseRepository courseRepository;

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN') " +
            "or hasRole('ROLE_TEACHER') and #course.curator.id == principal.teacher.id")
    public void delete(Course course) {
        courseRepository.delete(course);
    }
}
