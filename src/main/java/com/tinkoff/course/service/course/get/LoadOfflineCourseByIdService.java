package com.tinkoff.course.service.course.get;

import com.tinkoff.course.model.entity.course.OfflineCourse;

import javax.transaction.Transactional;

public interface LoadOfflineCourseByIdService {
    @Transactional
    OfflineCourse load(Long id);
}
