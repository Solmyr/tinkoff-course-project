package com.tinkoff.course.service.course.update;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.Course;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UpdateCourseServiceImpl implements UpdateCourseService {
    private final Map<CourseType, UpdateCourseDetailsService> updateCourseDetailsServiceMap;

    public UpdateCourseServiceImpl(
            List<UpdateCourseDetailsService> updateCourseDetailsServices
    ) {
        this.updateCourseDetailsServiceMap = new HashMap<>();
        for (UpdateCourseDetailsService service : updateCourseDetailsServices) {
            updateCourseDetailsServiceMap.put(service.getCourseType(), service);
        }
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN') " +
            "or hasRole('ROLE_TEACHER') and #course.curator.id == principal.teacher.id")
    public void update(
            Course course,
            String name,
            String description,
            Integer studentsNumber,
            Category category,
            Teacher curator,
            Map<String, Object> details
    ) {
        if (name != null) {
            course.setName(name);
        }

        if (description != null) {
            course.setDescription(description);
        }

        if (studentsNumber != null) {
            course.setStudentsNumber(studentsNumber);
        }

        if (category != null) {
            course.setCategory(category);
        }

        if (curator != null) {
            course.setCurator(curator);
        }

        updateCourseDetailsServiceMap.get(course.getType()).update(course.getId(), details);
    }
}
