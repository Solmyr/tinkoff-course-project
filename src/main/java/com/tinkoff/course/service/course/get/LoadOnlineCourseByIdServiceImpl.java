package com.tinkoff.course.service.course.get;

import com.tinkoff.course.model.entity.course.OnlineCourse;
import com.tinkoff.course.repository.course.OnlineCourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class LoadOnlineCourseByIdServiceImpl implements LoadOnlineCourseByIdService {
    private final OnlineCourseRepository onlineCourseRepository;

    @Override
    @Transactional
    public OnlineCourse load(Long id) {
        return onlineCourseRepository.findById(id).get();
    }
}
