package com.tinkoff.course.service.course.update;

import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.Course;
import com.tinkoff.course.validation.constraint.UpdateCourseTypeConstraint;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import java.util.Map;

@Validated
public interface UpdateCourseService {
    @Transactional
    @UpdateCourseTypeConstraint
    void update(
            Course course,
            String name,
            String description,
            Integer studentsNumber,
            Category category,
            Teacher curator,
            Map<String, Object> details
    );
}
