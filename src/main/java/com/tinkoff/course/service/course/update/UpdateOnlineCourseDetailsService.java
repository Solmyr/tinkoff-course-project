package com.tinkoff.course.service.course.update;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.course.OnlineCourse;
import com.tinkoff.course.repository.course.OnlineCourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class UpdateOnlineCourseDetailsService implements UpdateCourseDetailsService {
    private final OnlineCourseRepository onlineCourseRepository;

    @Override
    @Transactional
    public void update(Long id, Map<String, Object> details) {
        OnlineCourse course = onlineCourseRepository.getById(id);
        String platformLink = (String) details.get("platformLink");
        course.setPlatformLink(platformLink);
    }

    @Override
    public CourseType getCourseType() {
        return CourseType.ONLINE;
    }
}
