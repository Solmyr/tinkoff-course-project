package com.tinkoff.course.service.course.create;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.OnlineCourse;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class BuildOnlineCourseService implements BuildCourseService {
    public OnlineCourse create(
            String name,
            String description,
            Integer studentsNumber,
            Category category,
            Teacher curator,
            Map<String, Object> details
    ) {
        String platformLink = (String) details.get("platformLink");
        return new OnlineCourse(
                name,
                description,
                studentsNumber,
                category,
                curator,
                platformLink
        );
    }

    @Override
    public CourseType getCourseType() {
        return CourseType.ONLINE;
    }
}
