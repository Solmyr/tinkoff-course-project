package com.tinkoff.course.service.course.get.details;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.dto.CourseDto;

public interface GetTypedCourseByIdService {
    CourseDto get(Long id);
    CourseType getCourseType();
}
