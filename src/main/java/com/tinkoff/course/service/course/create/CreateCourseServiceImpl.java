package com.tinkoff.course.service.course.create;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.Course;
import com.tinkoff.course.repository.course.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CreateCourseServiceImpl implements CreateCourseService {
    private final Map<CourseType, BuildCourseService> buildCourseServicesMap;
    private final CourseRepository courseRepository;

    @Autowired
    public CreateCourseServiceImpl(
            List<BuildCourseService> buildCourseServices,
            CourseRepository courseRepository
    ) {
        this.courseRepository = courseRepository;
        buildCourseServicesMap = new HashMap<>();
        for (BuildCourseService buildCourseService : buildCourseServices) {
            buildCourseServicesMap.put(buildCourseService.getCourseType(), buildCourseService);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN') " +
            "or hasRole('ROLE_TEACHER') and #curator.id == principal.teacher.id")
    public Course create(
            String name,
            String description,
            Integer studentsNumber,
            Category category,
            Teacher curator,
            CourseType courseType,
            Map<String, Object> details
    ) {
        Course course = buildCourseServicesMap.get(courseType).create(
                name, description, studentsNumber, category, curator, details
        );
        return courseRepository.save(course);
    }
}
