package com.tinkoff.course.service.course.get;

import com.tinkoff.course.model.entity.course.Course;

public interface GetCourseByIdService {
    Course get(Long id);
}
