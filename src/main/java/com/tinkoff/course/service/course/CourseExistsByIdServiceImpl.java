package com.tinkoff.course.service.course;

import com.tinkoff.course.repository.course.CourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CourseExistsByIdServiceImpl implements CourseExistsByIdService {
    private final CourseRepository courseRepository;

    @Override
    public boolean exists(Long id) {
        return courseRepository.existsById(id);
    }
}
