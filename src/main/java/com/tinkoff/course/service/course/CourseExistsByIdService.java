package com.tinkoff.course.service.course;

public interface CourseExistsByIdService {
    boolean exists(Long id);
}
