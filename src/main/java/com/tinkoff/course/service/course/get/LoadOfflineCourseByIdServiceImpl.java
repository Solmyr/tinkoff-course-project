package com.tinkoff.course.service.course.get;

import com.tinkoff.course.model.entity.course.OfflineCourse;
import com.tinkoff.course.repository.course.OfflineCourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class LoadOfflineCourseByIdServiceImpl implements LoadOfflineCourseByIdService {
    private final OfflineCourseRepository offlineCourseRepository;

    @Override
    @Transactional
    public OfflineCourse load(Long id) {
        return offlineCourseRepository.findById(id).get();
    }
}
