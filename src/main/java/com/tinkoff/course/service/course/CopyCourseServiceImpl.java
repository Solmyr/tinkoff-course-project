package com.tinkoff.course.service.course;

import com.tinkoff.course.model.dto.CourseDto;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.Course;
import com.tinkoff.course.service.category.GetCategoryByIdService;
import com.tinkoff.course.service.course.create.CreateCourseService;
import com.tinkoff.course.service.lesson.CopyLessonService;
import com.tinkoff.course.service.lesson.GetLessonListByIdService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CopyCourseServiceImpl implements CopyCourseService {
    private final CopyLessonService copyLessonService;
    private final CreateCourseService createCourseService;
    private final GetCategoryByIdService getCategoryByIdService;
    private final GetLessonListByIdService getLessonListByIdService;

    @Override
    public Course copy(CourseDto courseDto, Teacher newCurator) {
        Course courseCopy = createCourseService.create(
                courseDto.getName(),
                courseDto.getDescription(),
                courseDto.getStudentsNumber(),
                getCategoryByIdService.get(courseDto.getCategoryId()),
                newCurator,
                courseDto.getCourseType(),
                courseDto.getDetails()
        );
        getLessonListByIdService
                .get(courseDto.getLessonsIds())
                .forEach(
                        lesson -> copyLessonService.copy(lesson, courseCopy)
                );
        return courseCopy;
    }
}
