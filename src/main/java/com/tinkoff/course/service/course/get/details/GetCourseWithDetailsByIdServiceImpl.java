package com.tinkoff.course.service.course.get.details;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.dto.CourseDto;
import com.tinkoff.course.service.course.get.GetCourseTypeByIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GetCourseWithDetailsByIdServiceImpl implements GetCourseWithDetailsByIdService {
    private final Map<CourseType, GetTypedCourseByIdService> getTypedCourseByIdServiceMap;
    private final GetCourseTypeByIdService getCourseTypeByIdService;

    @Autowired
    public GetCourseWithDetailsByIdServiceImpl(
            List<GetTypedCourseByIdService> getTypedCourseByIdServices,
            GetCourseTypeByIdService getCourseTypeByIdService
    ) {
        this.getCourseTypeByIdService = getCourseTypeByIdService;
        getTypedCourseByIdServiceMap = new HashMap<>();
        for (GetTypedCourseByIdService service : getTypedCourseByIdServices) {
            getTypedCourseByIdServiceMap.put(service.getCourseType(), service);
        }
    }

    @Override
    public CourseDto get(Long id) {
        CourseType courseType = getCourseTypeByIdService.get(id);
        return getTypedCourseByIdServiceMap.get(courseType).get(id);
    }
}
