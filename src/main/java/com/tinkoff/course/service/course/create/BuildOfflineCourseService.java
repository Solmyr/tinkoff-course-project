package com.tinkoff.course.service.course.create;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.OfflineCourse;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class BuildOfflineCourseService implements BuildCourseService {
    @Override
    public OfflineCourse create(
            String name,
            String description,
            Integer studentsNumber,
            Category category,
            Teacher curator,
            Map<String, Object> details
    ) {
        String address = (String) details.get("address");
        return new OfflineCourse(
                name,
                description,
                studentsNumber,
                category,
                curator,
                address
        );
    }

    @Override
    public CourseType getCourseType() {
        return CourseType.OFFLINE;
    }
}
