package com.tinkoff.course.service.course.get;

import com.tinkoff.course.model.CourseType;

public interface GetCourseTypeByIdService {
    CourseType get(Long id);
}
