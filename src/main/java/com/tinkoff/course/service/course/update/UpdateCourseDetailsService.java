package com.tinkoff.course.service.course.update;

import com.tinkoff.course.model.CourseType;

import java.util.Map;

public interface UpdateCourseDetailsService {
    void update(Long id, Map<String, Object> details);
    CourseType getCourseType();
}
