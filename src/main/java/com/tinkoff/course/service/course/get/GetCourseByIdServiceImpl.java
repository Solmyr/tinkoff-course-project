package com.tinkoff.course.service.course.get;

import com.tinkoff.course.model.entity.course.Course;
import com.tinkoff.course.repository.course.CourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetCourseByIdServiceImpl implements GetCourseByIdService {
    private final CourseRepository courseRepository;

    @Override
    public Course get(Long id) {
        return courseRepository.getById(id);
    }
}
