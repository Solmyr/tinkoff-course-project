package com.tinkoff.course.service.course.get.details;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.dto.CourseDto;
import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.course.OnlineCourse;
import com.tinkoff.course.service.course.get.LoadOnlineCourseByIdService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class GetOnlineCourseDtoByIdService implements GetTypedCourseByIdService {
    private final LoadOnlineCourseByIdService loadOnlineCourseByIdService;

    @Override
    public CourseDto get(Long id) {
        OnlineCourse course = loadOnlineCourseByIdService.load(id);
        return new CourseDto(
                course.getName(),
                course.getDescription(),
                course.getStudentsNumber(),
                course.getCategory().getId(),
                course.getCurator().getId(),
                course.getLessons().stream().map(Lesson::getId).toList(),
                getCourseType(),
                Map.of("platformLink", course.getPlatformLink())
        );
    }

    @Override
    public CourseType getCourseType() {
        return CourseType.ONLINE;
    }
}
