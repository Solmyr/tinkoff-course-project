package com.tinkoff.course.service.course.create;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.Course;
import com.tinkoff.course.validation.constraint.CreateCourseTypeConstraint;
import org.springframework.validation.annotation.Validated;

import java.util.Map;

@Validated
public interface CreateCourseService {
    @CreateCourseTypeConstraint
    Course create(
            String name,
            String description,
            Integer studentsNumber,
            Category category,
            Teacher curator,
            CourseType courseType,
            Map<String, Object> details
    );
}
