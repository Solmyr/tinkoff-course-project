package com.tinkoff.course.service.course.update;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.entity.course.OfflineCourse;
import com.tinkoff.course.repository.course.OfflineCourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class UpdateOfflineCourseDetailsService implements UpdateCourseDetailsService {
    private final OfflineCourseRepository offlineCourseRepository;

    @Override
    @Transactional
    public void update(Long id, Map<String, Object> details) {
        OfflineCourse course = offlineCourseRepository.getById(id);
        String address = (String) details.get("address");
        course.setAddress(address);
    }

    @Override
    public CourseType getCourseType() {
        return CourseType.OFFLINE;
    }
}
