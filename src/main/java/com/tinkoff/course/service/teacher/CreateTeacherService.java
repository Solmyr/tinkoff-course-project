package com.tinkoff.course.service.teacher;

import com.tinkoff.course.model.entity.Teacher;

import java.time.Period;

public interface CreateTeacherService {
    Teacher create(String fullName, Period age);
}
