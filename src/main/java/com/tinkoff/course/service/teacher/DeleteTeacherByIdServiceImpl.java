package com.tinkoff.course.service.teacher;

import com.tinkoff.course.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteTeacherByIdServiceImpl implements DeleteTeacherByIdService {
    private final TeacherRepository teacherRepository;

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_TEACHER') and #id == principal.teacher.id")
    public void delete(Long id) {
        teacherRepository.deleteById(id);
    }
}
