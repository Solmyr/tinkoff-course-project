package com.tinkoff.course.service.teacher;

import com.tinkoff.course.model.entity.Teacher;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Period;

@Service
@RequiredArgsConstructor
public class UpdateTeacherServiceImpl implements UpdateTeacherService {
    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_TEACHER') and #teacher.id == principal.teacher.id")
    public void update(Teacher teacher, String newFullName, Period newAge) {
        if (newFullName != null) {
            teacher.setFullName(newFullName);
        }
        if (newAge != null) {
            teacher.setAge(newAge);
        }
    }
}
