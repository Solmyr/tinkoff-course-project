package com.tinkoff.course.service.teacher;

public interface DeleteTeacherByIdService {
    void delete(Long id);
}
