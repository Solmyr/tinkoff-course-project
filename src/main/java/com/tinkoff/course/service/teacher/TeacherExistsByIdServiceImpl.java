package com.tinkoff.course.service.teacher;

import com.tinkoff.course.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TeacherExistsByIdServiceImpl implements TeacherExistsByIdService {
    private final TeacherRepository teacherRepository;

    @Override
    public boolean exists(Long id) {
        return teacherRepository.existsById(id);
    }
}
