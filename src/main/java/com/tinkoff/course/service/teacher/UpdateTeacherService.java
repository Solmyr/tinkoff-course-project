package com.tinkoff.course.service.teacher;

import com.tinkoff.course.model.entity.Teacher;

import javax.transaction.Transactional;
import java.time.Period;

public interface UpdateTeacherService {
    @Transactional
    void update(Teacher teacher, String newFullName, Period newAge);
}
