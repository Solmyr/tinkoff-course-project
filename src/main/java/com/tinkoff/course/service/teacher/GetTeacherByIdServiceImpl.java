package com.tinkoff.course.service.teacher;

import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetTeacherByIdServiceImpl implements GetTeacherByIdService {
    private final TeacherRepository teacherRepository;

    @Override
    public Teacher get(Long id) {
        return teacherRepository.getById(id);
    }
}
