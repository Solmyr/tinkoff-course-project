package com.tinkoff.course.service.teacher;

import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.time.Period;

@Service
@RequiredArgsConstructor
public class CreateTeacherServiceImpl implements CreateTeacherService {
    private final TeacherRepository teacherRepository;

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Teacher create(String fullName, Period age) {
        return teacherRepository.save(
                new Teacher(fullName,age)
        );
    }
}
