package com.tinkoff.course.service.teacher;

import com.tinkoff.course.model.entity.Teacher;

public interface LoadTeacherByIdService {
    Teacher load(Long id);
}
