package com.tinkoff.course.service.teacher;

import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoadTeacherByIdServiceImpl implements LoadTeacherByIdService {
    private final TeacherRepository teacherRepository;

    @Override
    public Teacher load(Long id) {
        return teacherRepository.findById(id).get();
    }
}
