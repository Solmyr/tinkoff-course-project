package com.tinkoff.course.service.teacher;

public interface TeacherExistsByIdService {
    boolean exists(Long id);
}
