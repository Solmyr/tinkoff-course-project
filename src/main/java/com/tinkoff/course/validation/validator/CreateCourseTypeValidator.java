package com.tinkoff.course.validation.validator;

import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.validation.constraint.CreateCourseTypeConstraint;
import com.tinkoff.course.validation.service.ValidateCourseDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;
import java.util.Map;

@Component
@RequiredArgsConstructor
@SupportedValidationTarget(ValidationTarget.PARAMETERS)
public class CreateCourseTypeValidator
        implements ConstraintValidator<CreateCourseTypeConstraint, Object[]> {
    private final ValidateCourseDetailsService validateCourseDetailsService;

    @Override
    public boolean isValid(Object[] objects, ConstraintValidatorContext constraintValidatorContext) {
        CourseType type = (CourseType) objects[5];
        Map<String, Object> details = (Map<String, Object>) objects[6];
        return validateCourseDetailsService.validate(type, details);
    }
}
