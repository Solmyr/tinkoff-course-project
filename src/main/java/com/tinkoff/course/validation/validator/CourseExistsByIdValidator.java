package com.tinkoff.course.validation.validator;

import com.tinkoff.course.service.course.CourseExistsByIdService;
import com.tinkoff.course.validation.constraint.CourseExistsByIdConstraint;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@RequiredArgsConstructor
public class CourseExistsByIdValidator
        implements ConstraintValidator<CourseExistsByIdConstraint, Long> {
    private final CourseExistsByIdService courseExistsByIdService;

    @Override
    public boolean isValid(Long courseId, ConstraintValidatorContext constraintValidatorContext) {
        if (courseId == null) {
            return true;
        }
        return courseExistsByIdService.exists(courseId);
    }
}
