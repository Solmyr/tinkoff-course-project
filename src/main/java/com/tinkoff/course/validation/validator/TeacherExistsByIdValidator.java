package com.tinkoff.course.validation.validator;

import com.tinkoff.course.service.teacher.TeacherExistsByIdService;
import com.tinkoff.course.validation.constraint.TeacherExistsByIdConstraint;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@RequiredArgsConstructor
public class TeacherExistsByIdValidator implements ConstraintValidator<TeacherExistsByIdConstraint, Long> {
    private final TeacherExistsByIdService teacherExistsByIdService;

    @Override
    public boolean isValid(Long teacherId, ConstraintValidatorContext constraintValidatorContext) {
        if (teacherId == null) {
            return true;
        }
        return teacherExistsByIdService.exists(teacherId);
    }
}
