package com.tinkoff.course.validation.validator;

import com.tinkoff.course.service.category.CategoryExistsByIdService;
import com.tinkoff.course.validation.constraint.CategoryExistsByIdConstraint;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@RequiredArgsConstructor
public class CategoryExistsByIdValidator
        implements ConstraintValidator<CategoryExistsByIdConstraint, Long> {
    private final CategoryExistsByIdService categoryExistsByIdService;

    @Override
    public boolean isValid(Long categoryId, ConstraintValidatorContext context) {
        if (categoryId == null) {
            return true;
        }
        return categoryExistsByIdService.exists(categoryId);
    }
}
