package com.tinkoff.course.validation.validator;

import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.validation.constraint.UpdateCategoryParentConstraint;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;

@Component
@RequiredArgsConstructor
@SupportedValidationTarget(ValidationTarget.PARAMETERS)
public class UpdateCategoryParentValidator
        implements ConstraintValidator<UpdateCategoryParentConstraint, Object[]> {

    @Override
    public boolean isValid(Object[] objects, ConstraintValidatorContext constraintValidatorContext) {
        if (objects[2] == null) {
            return true;
        }
        Category category = (Category) objects[0];
        Category parentCategory = (Category) objects[2];
        while (parentCategory != null) {
            if (category.equals(parentCategory)) {
                return false;
            }
            parentCategory = parentCategory.getParentCategory();
        }
        return true;
    }
}
