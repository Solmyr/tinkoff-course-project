package com.tinkoff.course.validation.validator;

import com.tinkoff.course.service.lesson.LessonExistsByIdService;
import com.tinkoff.course.validation.constraint.LessonExistsByIdConstraint;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@RequiredArgsConstructor
public class LessonExistsByIdValidator
        implements ConstraintValidator<LessonExistsByIdConstraint, Long> {
    private final LessonExistsByIdService lessonExistsByIdService;

    @Override
    public boolean isValid(Long lessonId, ConstraintValidatorContext constraintValidatorContext) {
        return lessonExistsByIdService.exists(lessonId);
    }
}
