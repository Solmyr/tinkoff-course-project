package com.tinkoff.course.validation.constraint;

import com.tinkoff.course.validation.validator.UpdateCourseTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD})
@Retention(RUNTIME)
@Constraint(validatedBy = UpdateCourseTypeValidator.class)
public @interface UpdateCourseTypeConstraint {
    String message() default "The details must suit the course type";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
