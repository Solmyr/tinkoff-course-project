package com.tinkoff.course.validation.constraint;

import com.tinkoff.course.validation.validator.UpdateCategoryParentValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD})
@Retention(RUNTIME)
@Constraint(validatedBy = UpdateCategoryParentValidator.class)
public @interface UpdateCategoryParentConstraint {
    String message() default "The descendant tree contain cycles";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
