package com.tinkoff.course.validation.constraint;

import com.tinkoff.course.validation.validator.CreateCourseTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD})
@Retention(RUNTIME)
@Constraint(validatedBy = CreateCourseTypeValidator.class)
public @interface CreateCourseTypeConstraint {
    String message() default "The details must suit the course type";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}