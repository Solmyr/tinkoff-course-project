package com.tinkoff.course.validation.constraint;

import com.tinkoff.course.validation.validator.TeacherExistsByIdValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = TeacherExistsByIdValidator.class)
@Target({ ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface TeacherExistsByIdConstraint {
    String message() default "Teacher not found";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
