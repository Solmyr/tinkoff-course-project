package com.tinkoff.course.validation.constraint;

import com.tinkoff.course.validation.validator.CategoryExistsByIdValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CategoryExistsByIdValidator.class)
@Target({ ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CategoryExistsByIdConstraint {
    String message() default "Category not found";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
