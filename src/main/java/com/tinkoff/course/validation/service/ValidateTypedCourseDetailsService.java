package com.tinkoff.course.validation.service;

import com.tinkoff.course.model.CourseType;

import java.util.Map;

public interface ValidateTypedCourseDetailsService {
    boolean validate(Map<String, Object> details);
    CourseType getType();
}
