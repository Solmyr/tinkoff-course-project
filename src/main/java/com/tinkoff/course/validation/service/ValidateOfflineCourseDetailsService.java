package com.tinkoff.course.validation.service;

import com.tinkoff.course.model.CourseType;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ValidateOfflineCourseDetailsService implements ValidateTypedCourseDetailsService {
    @Override
    public boolean validate(Map<String, Object> details) {
        return details.containsKey("address");
    }

    @Override
    public CourseType getType() {
        return CourseType.OFFLINE;
    }
}
