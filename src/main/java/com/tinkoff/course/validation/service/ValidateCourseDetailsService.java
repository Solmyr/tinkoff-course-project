package com.tinkoff.course.validation.service;

import com.tinkoff.course.model.CourseType;

import java.util.Map;

public interface ValidateCourseDetailsService {
    boolean validate(CourseType courseType, Map<String, Object> details);
}
