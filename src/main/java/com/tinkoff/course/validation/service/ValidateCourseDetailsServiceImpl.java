package com.tinkoff.course.validation.service;

import com.tinkoff.course.model.CourseType;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ValidateCourseDetailsServiceImpl implements ValidateCourseDetailsService {
    private final Map<CourseType, ValidateTypedCourseDetailsService> validateTypedCourseDetailsServiceMap;

    public ValidateCourseDetailsServiceImpl(
            List<ValidateTypedCourseDetailsService> validateTypedCourseDetailsServices
    ) {
        this.validateTypedCourseDetailsServiceMap = new HashMap<>();
        for (ValidateTypedCourseDetailsService service : validateTypedCourseDetailsServices) {
            validateTypedCourseDetailsServiceMap.put(
                    service.getType(),
                    service
            );
        }
    }

    @Override
    public boolean validate(CourseType courseType, Map<String, Object> details) {
        return validateTypedCourseDetailsServiceMap.get(courseType).validate(details);
    }
}
