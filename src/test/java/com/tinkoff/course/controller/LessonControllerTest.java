package com.tinkoff.course.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinkoff.course.IntegrationTest;
import com.tinkoff.course.model.dto.LessonDto;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.OnlineCourse;
import com.tinkoff.course.model.request.CreateLessonRequest;
import com.tinkoff.course.model.request.UpdateLessonRequest;
import com.tinkoff.course.model.response.DefaultErrorResponse;
import com.tinkoff.course.repository.CategoryRepository;
import com.tinkoff.course.repository.LessonRepository;
import com.tinkoff.course.repository.TeacherRepository;
import com.tinkoff.course.repository.course.CourseRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class LessonControllerTest extends IntegrationTest {
    private final MockMvc mockMvc;
    private final CourseRepository courseRepository;
    private final TeacherRepository teacherRepository;
    private final LessonRepository lessonRepository;
    private final CategoryRepository categoryRepository;
    private final ObjectMapper objectMapper;

    @Autowired
    public LessonControllerTest(
            MockMvc mockMvc,
            CourseRepository courseRepository,
            TeacherRepository teacherRepository,
            LessonRepository lessonRepository,
            CategoryRepository categoryRepository, ObjectMapper objectMapper
    ) {
        this.mockMvc = mockMvc;
        this.courseRepository = courseRepository;
        this.teacherRepository = teacherRepository;
        this.lessonRepository = lessonRepository;
        this.categoryRepository = categoryRepository;
        this.objectMapper = objectMapper;
    }

    @AfterEach
    public void afterEach() {
        clearDb();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getLessonByIdTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OnlineCourse course = courseRepository.save(
                new OnlineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "link"
                )
        );
        Lesson lesson = lessonRepository.save(
                new Lesson(
                        "test",
                        "test",
                        curator,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        course
                )
        );

        String expectedResult = objectMapper.writeValueAsString(new LessonDto(
                lesson.getName(),
                lesson.getDescription(),
                lesson.getTeacher().getId(),
                lesson.getStartTime(),
                lesson.getCourse().getId()
        ));
        mockMvc
                .perform(get("/lesson/" + lesson.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResult));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createLessonTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OnlineCourse course = courseRepository.save(
                new OnlineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "link"
                )
        );

        CreateLessonRequest createLessonRequest = new CreateLessonRequest(
                "test",
                "test",
                curator.getId(),
                LocalDateTime.of(10, 10, 10, 10, 10),
                course.getId()
        );
        String requestBody = objectMapper.writeValueAsString(createLessonRequest);

        mockMvc
                .perform(
                        post("/lesson/")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody)
                )
                .andExpect(status().isCreated());

        Lesson createdLesson = lessonRepository.findById(1L).get();
        Assertions.assertNotNull(createdLesson);
        Assertions.assertEquals("test", createdLesson.getName());
        Assertions.assertEquals("test", createdLesson.getDescription());
        Assertions.assertEquals(curator, createdLesson.getTeacher());
        Assertions.assertEquals(course, createdLesson.getCourse());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteLessonTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OnlineCourse course = courseRepository.save(
                new OnlineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "link"
                )
        );
        Lesson lesson = lessonRepository.save(
                new Lesson(
                        "test",
                        "test",
                        curator,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        course
                )
        );

        mockMvc
                .perform(delete("/lesson/" + lesson.getId()))
                .andExpect(status().isOk());
        Assertions.assertTrue(lessonRepository.findById(lesson.getId()).isEmpty());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateLessonTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OnlineCourse course = courseRepository.save(
                new OnlineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "link"
                )
        );
        Lesson lesson = lessonRepository.save(
                new Lesson(
                        "test",
                        "test",
                        curator,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        course
                )
        );

        UpdateLessonRequest updateCourseRequest = new UpdateLessonRequest(
                "new name",
                "new description",
                curator.getId(),
                LocalDateTime.of(3, 3, 3, 3, 3)
        );
        String requestBody = objectMapper.writeValueAsString(updateCourseRequest);

        mockMvc
                .perform(put("/lesson/" + lesson.getId())
                        .contentType(APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk());

        Lesson changedLesson = lessonRepository.findById(lesson.getId()).get();
        Assertions.assertNotNull(changedLesson);
        Assertions.assertEquals("new name", changedLesson.getName());
        Assertions.assertEquals("new description", changedLesson.getDescription());
        Assertions.assertEquals(curator, changedLesson.getTeacher());
        Assertions.assertEquals(
                LocalDateTime.of(3, 3, 3, 3, 3),
                changedLesson.getStartTime()
        );
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getLessonNotFoundTest() throws Exception {
        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(List.of("Lesson not found"))
        );

        mockMvc
                .perform(get("/lesson/" + 1))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createLessonBadRequestTest() throws Exception {
        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(
                        List.of(
                                "Name is mandatory",
                                "Description is mandatory",
                                "Teacher id is mandatory",
                                "Start time is mandatory",
                                "Course id is mandatory"
                        )
                )
        );

        mockMvc
                .perform(
                        post("/lesson/")
                                .contentType(APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(Map.of()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    @WithUserDetails("teacher")
    void createLessonForbiddenWrongCourseTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OnlineCourse course = courseRepository.save(
                new OnlineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "link"
                )
        );

        CreateLessonRequest createLessonRequest = new CreateLessonRequest(
                "test",
                "test",
                curator.getId(),
                LocalDateTime.of(10, 10, 10, 10, 10),
                course.getId()
        );
        String requestBody = objectMapper.writeValueAsString(createLessonRequest);

        mockMvc
                .perform(
                        post("/lesson/")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody)
                )
                .andExpect(status().isForbidden());
    }
}
