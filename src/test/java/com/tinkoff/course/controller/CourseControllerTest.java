package com.tinkoff.course.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinkoff.course.IntegrationTest;
import com.tinkoff.course.model.CourseType;
import com.tinkoff.course.model.dto.CourseDto;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.User;
import com.tinkoff.course.model.entity.course.OfflineCourse;
import com.tinkoff.course.model.entity.course.OnlineCourse;
import com.tinkoff.course.model.request.CreateCourseRequest;
import com.tinkoff.course.model.request.UpdateCourseRequest;
import com.tinkoff.course.model.response.CreateCourseResponse;
import com.tinkoff.course.model.response.DefaultErrorResponse;
import com.tinkoff.course.repository.CategoryRepository;
import com.tinkoff.course.repository.LessonRepository;
import com.tinkoff.course.repository.TeacherRepository;
import com.tinkoff.course.repository.course.CourseRepository;
import com.tinkoff.course.repository.course.OfflineCourseRepository;
import com.tinkoff.course.repository.course.OnlineCourseRepository;
import com.tinkoff.course.service.course.get.LoadOfflineCourseByIdService;
import com.tinkoff.course.service.course.get.LoadOnlineCourseByIdService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class CourseControllerTest extends IntegrationTest {
    private final MockMvc mockMvc;
    private final CategoryRepository categoryRepository;
    private final CourseRepository courseRepository;
    private final OnlineCourseRepository onlineCourseRepository;
    private final OfflineCourseRepository offlineCourseRepository;
    private final TeacherRepository teacherRepository;
    private final ObjectMapper objectMapper;
    private final UserDetailsService userDetailsService;
    private final LessonRepository lessonRepository;
    private final LoadOnlineCourseByIdService loadOnlineCourseByIdService;
    private final LoadOfflineCourseByIdService loadOfflineCourseByIdService;

    @Autowired
    public CourseControllerTest(
            MockMvc mockMvc,
            CategoryRepository categoryRepository,
            CourseRepository courseRepository,
            OnlineCourseRepository onlineCourseRepository,
            OfflineCourseRepository offlineCourseRepository,
            TeacherRepository teacherRepository,
            ObjectMapper objectMapper,
            UserDetailsService userDetailsService,
            LessonRepository lessonRepository,
            LoadOnlineCourseByIdService loadOnlineCourseByIdService,
            LoadOfflineCourseByIdService loadOfflineCourseByIdService
    ) {
        this.mockMvc = mockMvc;
        this.categoryRepository = categoryRepository;
        this.courseRepository = courseRepository;
        this.onlineCourseRepository = onlineCourseRepository;
        this.offlineCourseRepository = offlineCourseRepository;
        this.teacherRepository = teacherRepository;
        this.objectMapper = objectMapper;
        this.userDetailsService = userDetailsService;
        this.lessonRepository = lessonRepository;
        this.loadOnlineCourseByIdService = loadOnlineCourseByIdService;
        this.loadOfflineCourseByIdService = loadOfflineCourseByIdService;
    }

    @AfterEach
    public void afterEach() {
        clearDb();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getOnlineCourseByIdTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OnlineCourse onlineCourse = courseRepository.save(
                new OnlineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "link"
                )
        );
        Lesson lesson1 = lessonRepository.save(
                new Lesson(
                        "test",
                        "test",
                        curator,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        onlineCourse
                )
        );
        Lesson lesson2 = lessonRepository.save(
                new Lesson(
                        "test",
                        "test",
                        curator,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        onlineCourse
                )
        );
        String expectedOnlineCourse = objectMapper.writeValueAsString(new CourseDto(
                onlineCourse.getName(),
                onlineCourse.getDescription(),
                onlineCourse.getStudentsNumber(),
                onlineCourse.getCategory().getId(),
                onlineCourse.getCurator().getId(),
                List.of(lesson1.getId(), lesson2.getId()),
                CourseType.ONLINE,
                Map.of("platformLink", onlineCourse.getPlatformLink())
        ));
        mockMvc
                .perform(get("/course/" + onlineCourse.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedOnlineCourse));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getOfflineCourseByIdTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OfflineCourse offlineCourse = courseRepository.save(
                new OfflineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "address"
                )
        );
        Lesson lesson1 = lessonRepository.save(
                new Lesson(
                        "test",
                        "test",
                        curator,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        offlineCourse
                )
        );
        Lesson lesson2 = lessonRepository.save(
                new Lesson(
                        "test",
                        "test",
                        curator,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        offlineCourse
                )
        );
        String expectedOnlineCourse = objectMapper.writeValueAsString(new CourseDto(
                offlineCourse.getName(),
                offlineCourse.getDescription(),
                offlineCourse.getStudentsNumber(),
                offlineCourse.getCategory().getId(),
                offlineCourse.getCurator().getId(),
                List.of(lesson1.getId(), lesson2.getId()),
                CourseType.OFFLINE,
                Map.of("address", offlineCourse.getAddress())
        ));
        mockMvc
                .perform(get("/course/" + offlineCourse.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedOnlineCourse));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createOnlineCourseTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        CreateCourseRequest createCourseRequest = new CreateCourseRequest(
                "test",
                "test",
                10,
                category.getId(),
                curator.getId(),
                CourseType.ONLINE,
                Map.of("platformLink", "link")
        );
        String createCourseRequestBody = objectMapper.writeValueAsString(createCourseRequest);

        mockMvc
                .perform(
                        post("/course/")
                                .contentType(APPLICATION_JSON)
                                .content(createCourseRequestBody)
                )
                .andExpect(status().isCreated());

        OnlineCourse onlineCourse = onlineCourseRepository.findById(1L).get();
        Assertions.assertNotNull(onlineCourse);
        Assertions.assertEquals("test", onlineCourse.getName());
        Assertions.assertEquals("test", onlineCourse.getDescription());
        Assertions.assertEquals(10, onlineCourse.getStudentsNumber());
        Assertions.assertEquals(category, onlineCourse.getCategory());
        Assertions.assertEquals(curator, onlineCourse.getCurator());
        Assertions.assertEquals("link", onlineCourse.getPlatformLink());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createOfflineCourseTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        CreateCourseRequest createCourseRequest = new CreateCourseRequest(
                "test",
                "test",
                10,
                category.getId(),
                curator.getId(),
                CourseType.OFFLINE,
                Map.of("address", "address")
        );
        String createCourseRequestBody = objectMapper.writeValueAsString(createCourseRequest);

        mockMvc
                .perform(
                        post("/course/")
                                .contentType(APPLICATION_JSON)
                                .content(createCourseRequestBody)
                )
                .andExpect(status().isCreated());

        OfflineCourse offlineCourse = offlineCourseRepository.findById(1L).get();
        Assertions.assertNotNull(offlineCourse);
        Assertions.assertEquals("test", offlineCourse.getName());
        Assertions.assertEquals("test", offlineCourse.getDescription());
        Assertions.assertEquals(10, offlineCourse.getStudentsNumber());
        Assertions.assertEquals(category, offlineCourse.getCategory());
        Assertions.assertEquals(curator, offlineCourse.getCurator());
        Assertions.assertEquals("address", offlineCourse.getAddress());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateOnlineCourseTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OnlineCourse onlineCourse = courseRepository.save(
                new OnlineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "link"
                )
        );

        UpdateCourseRequest updateCourseRequest = new UpdateCourseRequest(
                "new name",
                "new description",
                88,
                category.getId(),
                curator.getId(),
                Map.of("platformLink", "new link")
        );
        String requestBody = objectMapper.writeValueAsString(updateCourseRequest);

        mockMvc
                .perform(put("/course/" + onlineCourse.getId())
                        .contentType(APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk());


        OnlineCourse updatedCourse = onlineCourseRepository.findById(onlineCourse.getId()).get();
        Assertions.assertNotNull(updatedCourse);
        Assertions.assertEquals("new name", updatedCourse.getName());
        Assertions.assertEquals("new description", updatedCourse.getDescription());
        Assertions.assertEquals(88, updatedCourse.getStudentsNumber());
        Assertions.assertEquals(category, updatedCourse.getCategory());
        Assertions.assertEquals(curator, updatedCourse.getCurator());
        Assertions.assertEquals("new link", updatedCourse.getPlatformLink());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateOfflineCourseTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OfflineCourse offlineCourse = offlineCourseRepository.save(
                new OfflineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "address"
                )
        );

        UpdateCourseRequest updateCourseRequest = new UpdateCourseRequest(
                "new name",
                "new description",
                88,
                category.getId(),
                curator.getId(),
                Map.of("address", "new address")
        );
        String requestBody = objectMapper.writeValueAsString(updateCourseRequest);

        mockMvc
                .perform(put("/course/" + offlineCourse.getId())
                        .contentType(APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk());


        OfflineCourse updatedCourse = offlineCourseRepository.findById(offlineCourse.getId()).get();
        Assertions.assertNotNull(updatedCourse);
        Assertions.assertEquals("new name", updatedCourse.getName());
        Assertions.assertEquals("new description", updatedCourse.getDescription());
        Assertions.assertEquals(88, updatedCourse.getStudentsNumber());
        Assertions.assertEquals(category, updatedCourse.getCategory());
        Assertions.assertEquals(curator, updatedCourse.getCurator());
        Assertions.assertEquals("new address", updatedCourse.getAddress());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteCourseTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OnlineCourse onlineCourse = courseRepository.save(
                new OnlineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "link"
                )
        );
        OfflineCourse offlineCourse = offlineCourseRepository.save(
                new OfflineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "address"
                )
        );

        mockMvc
                .perform(delete("/course/" + onlineCourse.getId()))
                .andExpect(status().isOk());
        mockMvc
                .perform(delete("/course/" + offlineCourse.getId()))
                .andExpect(status().isOk());

        Assertions.assertTrue(courseRepository.findById(onlineCourse.getId()).isEmpty());
        Assertions.assertTrue(courseRepository.findById(offlineCourse.getId()).isEmpty());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getCourseNotFoundTest() throws Exception {
        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(List.of("Course not found"))
        );

        mockMvc
                .perform(get("/course/" + 1))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    @WithUserDetails("admin")
    void copyOnlineCourseTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OnlineCourse onlineCourse = courseRepository.save(
                new OnlineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "link"
                )
        );
        Lesson lesson = lessonRepository.save(
                new Lesson(
                        "test",
                        "test",
                        curator,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        onlineCourse
                )
        );

        String response = mockMvc
                .perform(post("/course/" + onlineCourse.getId() + "/copy"))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CreateCourseResponse createCourseResponse =
                objectMapper.readValue(response, CreateCourseResponse.class);

        OnlineCourse courseCopy = loadOnlineCourseByIdService.load(createCourseResponse.getId());
        Assertions.assertNotNull(courseCopy);
        Assertions.assertEquals("test", courseCopy.getName());
        Assertions.assertEquals("test", courseCopy.getDescription());
        Assertions.assertEquals(10, courseCopy.getStudentsNumber());
        Assertions.assertEquals(category, courseCopy.getCategory());
        Assertions.assertEquals(
                ((User) userDetailsService.loadUserByUsername("admin")).getTeacher(),
                courseCopy.getCurator()
        );
        Assertions.assertEquals("link", courseCopy.getPlatformLink());

        Lesson lessonCopy = courseCopy.getLessons().get(0);
        Assertions.assertEquals(lesson.getName(), lessonCopy.getName());
        Assertions.assertEquals(lesson.getDescription(), lessonCopy.getDescription());
        Assertions.assertEquals(courseCopy, lessonCopy.getCourse());
        Assertions.assertEquals(lesson.getTeacher(), lessonCopy.getTeacher());
        Assertions.assertEquals(
                LocalDateTime.of(1, 1, 1, 0, 0),
                lessonCopy.getStartTime()
        );
    }

    @Test
    @WithUserDetails("admin")
    void copyOfflineCourseTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        OfflineCourse offlineCourse = offlineCourseRepository.save(
                new OfflineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        curator,
                        "address"
                )
        );
        Lesson lesson = lessonRepository.save(
                new Lesson(
                        "test",
                        "test",
                        curator,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        offlineCourse
                )
        );

        String response = mockMvc
                .perform(post("/course/" + offlineCourse.getId() + "/copy"))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CreateCourseResponse createCourseResponse =
                objectMapper.readValue(response, CreateCourseResponse.class);

        OfflineCourse courseCopy = loadOfflineCourseByIdService.load(createCourseResponse.getId());
        Assertions.assertNotNull(courseCopy);
        Assertions.assertEquals("test", courseCopy.getName());
        Assertions.assertEquals("test", courseCopy.getDescription());
        Assertions.assertEquals(10, courseCopy.getStudentsNumber());
        Assertions.assertEquals(category, courseCopy.getCategory());
        Assertions.assertEquals(
                ((User) userDetailsService.loadUserByUsername("admin")).getTeacher(),
                courseCopy.getCurator()
        );
        Assertions.assertEquals("address", courseCopy.getAddress());

        Lesson lessonCopy = courseCopy.getLessons().get(0);
        Assertions.assertEquals(lesson.getName(), lessonCopy.getName());
        Assertions.assertEquals(lesson.getDescription(), lessonCopy.getDescription());
        Assertions.assertEquals(courseCopy, lessonCopy.getCourse());
        Assertions.assertEquals(lesson.getTeacher(), lessonCopy.getTeacher());
        Assertions.assertEquals(
                LocalDateTime.of(1, 1, 1, 0, 0),
                lessonCopy.getStartTime()
        );
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createCourseBadRequestFieldsNotSpecifiedTest() throws Exception {
        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(
                        List.of(
                                "Name is mandatory",
                                "Description is mandatory",
                                "Students number grade is mandatory",
                                "Category id is mandatory",
                                "Curator id is mandatory",
                                "Course type is mandatory",
                                "Details field is mandatory"
                        )
                )
        );

        mockMvc
                .perform(
                        post("/course/")
                                .contentType(APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(Map.of()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createOnlineCourseBadRequestWrongDetailsTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        CreateCourseRequest createCourseRequest = new CreateCourseRequest(
                "test",
                "test",
                10,
                category.getId(),
                curator.getId(),
                CourseType.ONLINE,
                Map.of()
        );
        String createCourseRequestBody = objectMapper.writeValueAsString(createCourseRequest);

        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(List.of("The details must suit the course type"))
        );

        mockMvc
                .perform(
                        post("/course/")
                                .contentType(APPLICATION_JSON)
                                .content(createCourseRequestBody)
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createOfflineCourseBadRequestWrongDetailsTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        CreateCourseRequest createCourseRequest = new CreateCourseRequest(
                "test",
                "test",
                10,
                category.getId(),
                curator.getId(),
                CourseType.OFFLINE,
                Map.of()
        );
        String createCourseRequestBody = objectMapper.writeValueAsString(createCourseRequest);

        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(List.of("The details must suit the course type"))
        );

        mockMvc
                .perform(
                        post("/course/")
                                .contentType(APPLICATION_JSON)
                                .content(createCourseRequestBody)
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    @WithUserDetails("teacher")
    void createCourseForbiddenCuratorTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));
        Teacher curator = teacherRepository.save(new Teacher("test", Period.ofYears(30)));
        CreateCourseRequest createCourseRequest = new CreateCourseRequest(
                "test",
                "test",
                10,
                category.getId(),
                curator.getId(),
                CourseType.OFFLINE,
                Map.of()
        );
        String createCourseRequestBody = objectMapper.writeValueAsString(createCourseRequest);

        mockMvc
                .perform(
                        post("/course/")
                                .contentType(APPLICATION_JSON)
                                .content(createCourseRequestBody)
                )
                .andExpect(status().isForbidden());
    }
}
