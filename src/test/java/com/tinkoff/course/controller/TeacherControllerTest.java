package com.tinkoff.course.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinkoff.course.IntegrationTest;
import com.tinkoff.course.model.dto.LessonDto;
import com.tinkoff.course.model.dto.TeacherDto;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.entity.Lesson;
import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.model.entity.course.OfflineCourse;
import com.tinkoff.course.model.request.CreateTeacherRequest;
import com.tinkoff.course.model.request.UpdateTeacherRequest;
import com.tinkoff.course.model.response.CreateTeacherResponse;
import com.tinkoff.course.model.response.DefaultErrorResponse;
import com.tinkoff.course.model.response.GetTeacherScheduleResponse;
import com.tinkoff.course.repository.CategoryRepository;
import com.tinkoff.course.repository.LessonRepository;
import com.tinkoff.course.repository.TeacherRepository;
import com.tinkoff.course.repository.course.CourseRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class TeacherControllerTest extends IntegrationTest {
    private final MockMvc mockMvc;
    private final TeacherRepository teacherRepository;
    private final LessonRepository lessonRepository;
    private final CategoryRepository categoryRepository;
    private final CourseRepository courseRepository;
    private final ObjectMapper objectMapper;

    @Autowired
    public TeacherControllerTest(
            MockMvc mockMvc,
            TeacherRepository teacherRepository,
            LessonRepository lessonRepository,
            CategoryRepository categoryRepository,
            CourseRepository courseRepository,
            ObjectMapper objectMapper
    ) {
        this.mockMvc = mockMvc;
        this.teacherRepository = teacherRepository;
        this.lessonRepository = lessonRepository;
        this.categoryRepository = categoryRepository;
        this.courseRepository = courseRepository;
        this.objectMapper = objectMapper;
    }

    @AfterEach
    public void afterEach() {
        clearDb();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getTeacherByIdTest() throws Exception {
        Teacher teacher = teacherRepository.save(new Teacher("test", Period.ofYears(10)));

        String expectedResult = objectMapper.writeValueAsString(new TeacherDto(
                "test",
                Period.ofYears(10)
        ));

        mockMvc
                .perform(get("/teacher/" + teacher.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResult));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createTeacherTest() throws Exception {
        CreateTeacherRequest createTeacherRequest = new CreateTeacherRequest(
                "test", Period.ofYears(10)
        );
        String requestBody = objectMapper.writeValueAsString(createTeacherRequest);

        String response = mockMvc
                .perform(
                        post("/teacher/")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody)
                )
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CreateTeacherResponse createTeacherResponse =
                objectMapper.readValue(response, CreateTeacherResponse.class);

        Teacher teacher = teacherRepository.findById(createTeacherResponse.getId()).get();
        Assertions.assertNotNull(teacher);
        Assertions.assertEquals("test", teacher.getFullName());
        Assertions.assertEquals(Period.ofYears(10), teacher.getAge());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteTeacherTest() throws Exception {
        Teacher teacher = teacherRepository.save(new Teacher("test", Period.ofYears(10)));

        mockMvc
                .perform(delete("/teacher/" + teacher.getId()))
                .andExpect(status().isOk());
        Assertions.assertTrue(teacherRepository.findById(teacher.getId()).isEmpty());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateTeacherTest() throws Exception {
        Teacher teacher = teacherRepository.save(new Teacher("test", Period.ofYears(10)));

        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(
                "new name", Period.ofYears(333)
        );
        String requestBody = objectMapper.writeValueAsString(updateTeacherRequest);

        mockMvc
                .perform(put("/teacher/" + teacher.getId())
                        .contentType(APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk());

        Teacher changedTeacher = teacherRepository.findById(teacher.getId()).get();
        Assertions.assertNotNull(changedTeacher);
        Assertions.assertEquals("new name", changedTeacher.getFullName());
        Assertions.assertEquals(Period.ofYears(333), changedTeacher.getAge());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getTeacherNotFoundTest() throws Exception {
        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(List.of("Teacher not found"))
        );

        mockMvc
                .perform(get("/teacher/" + 101))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createTeacherBadRequestTest() throws Exception {
        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(List.of("Name is mandatory", "Age is mandatory"))
        );

        mockMvc
                .perform(
                        post("/teacher/")
                                .contentType(APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(Map.of()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    void createTeacherUnauthorizedTest() throws Exception {
        mockMvc
                .perform(
                        post("/teacher/")
                                .contentType(APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(Map.of()))
                )
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "TEACHER")
    void createTeacherForbiddenTest() throws Exception {
        CreateTeacherRequest createTeacherRequest = new CreateTeacherRequest(
                "test", Period.ofYears(10)
        );
        String requestBody = objectMapper.writeValueAsString(createTeacherRequest);

        mockMvc
                .perform(
                        post("/teacher/")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody)
                )
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getTeacherScheduleTest() throws Exception {
        Teacher teacher = teacherRepository.save(new Teacher("test", Period.ofYears(10)));
        Category category = categoryRepository.save(new Category("test"));
        OfflineCourse offlineCourse = courseRepository.save(
                new OfflineCourse(
                        "test",
                        "test",
                        10,
                        category,
                        teacher,
                        "address"
                )
        );
        Lesson lesson1 = lessonRepository.save(
                new Lesson(
                        "test1",
                        "test1",
                        teacher,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        offlineCourse
                )
        );
        Lesson lesson2 = lessonRepository.save(
                new Lesson(
                        "test2",
                        "test2",
                        teacher,
                        LocalDateTime.of(10, 10, 10, 10, 10),
                        offlineCourse
                )
        );

        String expectedResult = objectMapper.writeValueAsString(
                new GetTeacherScheduleResponse(
                        List.of(
                                new LessonDto(
                                        lesson1.getName(),
                                        lesson1.getDescription(),
                                        lesson1.getTeacher().getId(),
                                        lesson1.getStartTime(),
                                        lesson1.getCourse().getId()
                                ),
                                new LessonDto(
                                        lesson2.getName(),
                                        lesson2.getDescription(),
                                        lesson2.getTeacher().getId(),
                                        lesson2.getStartTime(),
                                        lesson2.getCourse().getId()
                                )
                        )
                )
        );

        mockMvc
                .perform(get("/teacher/" + teacher.getId() + "/schedule"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResult));
    }
}
