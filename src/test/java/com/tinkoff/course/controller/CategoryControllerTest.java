package com.tinkoff.course.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinkoff.course.IntegrationTest;
import com.tinkoff.course.model.dto.CategoryDto;
import com.tinkoff.course.model.entity.Category;
import com.tinkoff.course.model.request.CreateCategoryRequest;
import com.tinkoff.course.model.request.UpdateCategoryRequest;
import com.tinkoff.course.model.response.DefaultErrorResponse;
import com.tinkoff.course.repository.CategoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class CategoryControllerTest extends IntegrationTest {
    private final MockMvc mockMvc;
    private final CategoryRepository categoryRepository;
    private final ObjectMapper objectMapper;

    @Autowired
    public CategoryControllerTest(
            MockMvc mockMvc,
            CategoryRepository categoryRepository,
            ObjectMapper objectMapper
    ) {
        this.mockMvc = mockMvc;
        this.categoryRepository = categoryRepository;
        this.objectMapper = objectMapper;
    }

    @AfterEach
    public void afterEach() {
        clearDb();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getCategoryByIdTest() throws Exception {
        Category parentCategory = categoryRepository.save(new Category("parent"));
        Category childCategory = categoryRepository.save(new Category("child", parentCategory));
        String expectedParentCategoryResult = objectMapper.writeValueAsString(new CategoryDto(
                parentCategory.getName(),
                null
        ));
        String expectedChildCategoryResult = objectMapper.writeValueAsString(new CategoryDto(
                childCategory.getName(),
                childCategory.getParentCategory().getId()
        ));
        mockMvc
                .perform(get("/category/" + parentCategory.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedParentCategoryResult));
        mockMvc
                .perform(get("/category/" + childCategory.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedChildCategoryResult));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createCategoryTest() throws Exception {
        CreateCategoryRequest createParentCategoryRequest = new CreateCategoryRequest(
                "parent", null
        );
        String createParentRequestBody = objectMapper.writeValueAsString(createParentCategoryRequest);

        mockMvc
                .perform(
                        post("/category/")
                                .contentType(APPLICATION_JSON)
                                .content(createParentRequestBody)
                )
                .andExpect(status().isCreated());
        CreateCategoryRequest createChildCategoryRequest = new CreateCategoryRequest(
                "child", 1L
        );
        String createChildRequestBody = objectMapper.writeValueAsString(createChildCategoryRequest);

        mockMvc
                .perform(
                        post("/category/")
                                .contentType(APPLICATION_JSON)
                                .content(createChildRequestBody)
                )
                .andExpect(status().isCreated());

        Category expectedParentCategory = new Category("parent");
        expectedParentCategory.setId(1L);
        Assertions.assertEquals(expectedParentCategory, categoryRepository.getById(1L));

        Category expectedChildCategory = new Category("child");
        expectedChildCategory.setId(2L);
        Assertions.assertEquals(expectedChildCategory, categoryRepository.getById(2L));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteCategoryTest() throws Exception {
        Category category = categoryRepository.save(new Category("test"));

        mockMvc
                .perform(delete("/category/" + category.getId()))
                .andExpect(status().isOk());
        Assertions.assertTrue(categoryRepository.findById(category.getId()).isEmpty());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateCategoryTest() throws Exception {
        Category firstCategory = categoryRepository.save(new Category("first"));
        Category secondCategory = categoryRepository.save(new Category("second"));

        UpdateCategoryRequest updateCourseRequest = new UpdateCategoryRequest(
                "first changed", secondCategory.getId()
        );
        String requestBody = objectMapper.writeValueAsString(updateCourseRequest);

        mockMvc
                .perform(put("/category/" + firstCategory.getId())
                        .contentType(APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk());

        Category changedFirstCategory = categoryRepository.findById(firstCategory.getId()).get();
        Assertions.assertEquals("first changed", changedFirstCategory.getName());
        Assertions.assertEquals(secondCategory, changedFirstCategory.getParentCategory());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateCategoryDescendantTreeContainCyclesTest() throws Exception {
        Category firstCategory = categoryRepository.save(new Category("first"));
        Category secondCategory = categoryRepository.save(new Category("second", firstCategory));
        Category thirdCategory = categoryRepository.save(new Category("second", secondCategory));

        UpdateCategoryRequest updateCourseRequest = new UpdateCategoryRequest(
                "first changed", thirdCategory.getId()
        );
        String requestBody = objectMapper.writeValueAsString(updateCourseRequest);
        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(List.of("The descendant tree contain cycles"))
        );

        mockMvc
                .perform(put("/category/" + firstCategory.getId())
                        .contentType(APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getCategoryNotFoundTest() throws Exception {
        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(List.of("Category not found"))
        );

        mockMvc
                .perform(get("/category/" + 1))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createCategoryBadRequestTest() throws Exception {
        String expectedResponse = objectMapper.writeValueAsString(
                new DefaultErrorResponse(List.of("Name is mandatory"))
        );

        mockMvc
                .perform(
                        post("/category/")
                                .contentType(APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(Map.of()))
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResponse));
    }

    @Test
    void createCategoryUnauthorizedTest() throws Exception {
        mockMvc
                .perform(
                        post("/category/")
                                .contentType(APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(Map.of()))
                )
                .andExpect(status().isUnauthorized());
    }
}
