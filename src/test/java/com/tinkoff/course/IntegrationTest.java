package com.tinkoff.course;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class IntegrationTest {
    @Autowired
    private Flyway flyway;

    public void clearDb() {
        flyway.clean();
        flyway.migrate();
    }
}