package com.tinkoff.course;

import com.tinkoff.course.model.entity.Teacher;
import com.tinkoff.course.repository.TeacherRepository;
import com.tinkoff.course.service.teacher.GetTeacherByIdService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Period;

@SpringBootTest
public class ExampleUnitTest {
    @MockBean
    private final TeacherRepository teacherRepository;
    private final GetTeacherByIdService getTeacherByIdService;

    @Autowired
    public ExampleUnitTest(
            TeacherRepository teacherRepository,
            GetTeacherByIdService getTeacherByIdService
    ) {
        this.teacherRepository = teacherRepository;
        this.getTeacherByIdService = getTeacherByIdService;
    }

    @Test
    void getTeacherByIdOkTest() {
        final Long teacherId = 1L;
        final String fullName = "test";
        final Period age = Period.ofYears(111);
        Mockito.when(teacherRepository.getById(teacherId)).thenReturn(new Teacher(
                fullName, age
        ));

        Teacher teacher = getTeacherByIdService.get(teacherId);

        Assertions.assertEquals(fullName, teacher.getFullName());
        Assertions.assertEquals(age, teacher.getAge());
    }
}
