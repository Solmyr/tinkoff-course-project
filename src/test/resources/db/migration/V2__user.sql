insert into teacher (age, full_name)
values ('\xaced00057372000d6a6176612e74696d652e536572955d84ba1b2248b20c00007870770d0e0000000a000000000000000078', 'admin');

insert into app_user (email, password, role, teacher_id)
values ('admin', '$2a$12$Bh0DDKbg78XRmlp3scljv./F/Ctw1PjlozFljI3ox3jCkOvJKlg66', 'ROLE_ADMIN', 1);

insert into teacher (age, full_name)
values ('\xaced00057372000d6a6176612e74696d652e536572955d84ba1b2248b20c00007870770d0e0000000a000000000000000078', 'teacher');

insert into app_user (email, password, role, teacher_id)
values ('teacher', '$2a$12$Bh0DDKbg78XRmlp3scljv./F/Ctw1PjlozFljI3ox3jCkOvJKlg66', 'ROLE_TEACHER', 2);